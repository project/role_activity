
This module is for logging certain actions for certain roles on a busy
site. It is useful to see how active moderators are for example.
